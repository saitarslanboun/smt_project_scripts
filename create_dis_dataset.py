import codecs, sys

def morphanalyze(sfile_name, tfile_name):
        f = codecs.open(sfile_name, encoding="utf-8").read()
        target = open(tfile_name, "w")
        bags = f.split("\n\n")
        target.write("<DOC> <DOC>+BDTag\n")
        target.write("<S> <S>+BSTag\n")
        for a in range(len(bags)):
                print str(a+1) + " : " + str(len(bags))
                words = bags[a].split("\n")
                if words[0].split("\t")[0] == "%%%%%%%%%%":
                        if a != (len(bags) - 1):
                                target.write("</S> </S>+ESTag\n<S> <S>+BSTag\n".encode("utf-8"))
                        if a == (len(bags) - 1):
                                target.write("</S> </S>+ESTag\n</DOC> </DOC>+EDTag".encode("utf-8"))
                else:
                        line_str = ""
                        for b in range(len(words)):
                                line = words[b].split("\t")
                                if len(line) == 3:
                                        line_str += line[1] + line[2] + " "
                                if len(line) == 2:
                                        line_str += line[1] + " "
                        line_str = line[0] + " " + line_str[:-1] + "\n"
                        target.write(line_str.encode("utf-8"))

if __name__ == "__main__":
	print "Creating disambiguate dataset ..."
	sfile_name = sys.argv[1]
	tfile_name = sys.argv[2]
	morphanalyze(sfile_name, tfile_name)

