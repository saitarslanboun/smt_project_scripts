import codecs, sys

if __name__ == "__main__":
	print "Importing corpus..."
	sfile_name = sys.argv[1]
	tfile_name = sys.argv[2]
	f = codecs.open(sfile_name, encoding="utf-8").read()
	
	print "Extracting sentences..."
	sentences = f.split("\n\n")

	print "Creating target corpus ..."
	target = open(tfile_name, "w")
	for a in range(len(sentences)):
		print str(a+1) + " : " + str(len(sentences))
		sentence = sentences[a]
		words = sentence.split("\n")
		line = ""
		for b in range(len(words)):
			word = words[b]
			w_parts = word.split()
			w = w_parts[0]
			m = w_parts[1]
			m_parts = m.split("+")
			if len(m_parts) == 1:
				line += w + " "
			if len(m_parts) > 1:
				line += m + " "
		line += "\n"
		target.write(line.encode("utf-8"))
	target.close()
	
