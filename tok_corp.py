import codecs, sys

def morphanalyze(file_name, f):
	target = open(file_name, "w")
	lenf = len(f)
	for a in range(lenf):
		print str(a+1) + " : " + str(lenf)
		line = ""
		tokens = f[a].replace("\n", "").split()
		for b in range(len(tokens)):
			line += tokens[b] + "\n"
		line += "%%%%%%%%%%\n"
		target.write(line.encode("utf-8"))

if __name__ == "__main__":
	print "Importing corpus ..."
	sfile_name = sys.argv[1]
	tfile_name = sys.argv[2]
	f = codecs.open(sfile_name, encoding="utf-8").readlines()
	tfile_name = "../corpus/toks.tr"
	morphanalyze(tfile_name, f)
