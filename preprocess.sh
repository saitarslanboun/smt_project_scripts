# path to moses decoder: https://github.com/moses-smt/mosesdecoder
mosesdecoder=~/build/mosesdecoder

# normalize punctuation
#echo "Normalizing punctuation for source corpus"
#$mosesdecoder/scripts/tokenizer/normalize-punctuation.perl -l en < ../corpus/all.en > ../corpus/all_np.en
#echo "Normalizing punctuation for target corpus"
#$mosesdecoder/scripts/tokenizer/normalize-punctuation.perl -l tr < ../corpus/all.tr > ../corpus/all_np.tr

# tokenize
echo "Tokenizing source corpus"
$mosesdecoder/scripts/tokenizer/tokenizer.perl -threads 40 -l en -penn < ../corpus/all_np.en > ../corpus/all_np_tok.en
echo "Tokenizing target corpus"
$mosesdecoder/scripts/tokenizer/tokenizer.perl -threads	40 -l tr -penn < ../corpus/all_np.tr > ../corpus/all_np_tok.tr

# truecase
echo "Training trucase model for source corpus"
$mosesdecoder/scripts/recaser/train-truecaser.perl --model ~/SMT/corpus/truecase-model.en --corpus ~/SMT/corpus/all_np_tok.en
echo "Training trucase model for target corpus"
$mosesdecoder/scripts/recaser/train-truecaser.perl --model ~/SMT/corpus/truecase-model.tr --corpus ~/SMT/corpus/all_np_tok.tr
echo "Truecasing source corpus"
$mosesdecoder/scripts/recaser/truecase.perl --model ~/SMT/corpus/truecase-model.en < ~/SMT/corpus/all_np_tok.en > ~/SMT/corpus/all_np_tok_true.en
echo "Truecasing target corpus"
$mosesdecoder/scripts/recaser/truecase.perl --model ~/SMT/corpus/truecase-model.tr < ~/SMT/corpus/all_np_tok.tr > ~/SMT/corpus/all_np_tok_true.tr

